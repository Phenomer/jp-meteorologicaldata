#!/usr/bin/ruby
# coding: utf-8

require 'nokogiri'
require 'fileutils'
require 'open-uri'
require 'yaml'
require 'optparse'
require 'optparse/date'

class MeteorologicalData
  # 10分毎の気象データを取得
  # http://www.data.jma.go.jp/obd/stats/etrn/view/10min_a1.php?prec_no=35&block_no=1040&year=2019&month=8&day=15&view=
  
  CACHE_DIR = File.join(File.dirname(__FILE__), 'cache')
  BASE_S_URL  = 'http://www.data.jma.go.jp/obd/stats/etrn/view/10min_s1.php?'
  BASE_A_URL  = 'http://www.data.jma.go.jp/obd/stats/etrn/view/10min_a1.php?'
  #BASE_S_URL  = 'http://www.data.jma.go.jp/obd/stats/etrn/view/hourly_s1.php?'
  #BASE_A_URL  = 'http://www.data.jma.go.jp/obd/stats/etrn/view/hourly_a1.php?'

  def initialize
    FileUtils.mkpath(CACHE_DIR)
  end

  def get(prec_no, block_no, year, month, day, force: false)
    if not force and cached?(prec_no, block_no, year, month, day)
      data = get_html_cache(prec_no, block_no, year, month, day)
    else
      data = get_html(prec_no, block_no, year, month, day)
    end
    res = parse(data)
  end

  def get_csv(prec_no, block_no, year, month, day, force: false)
    contents = get(prec_no, block_no, year, month, day, force: force)
    unless contents[:header]
      raise "Data is broken - #{cache_path(prec_no, block_no, year, month, day)}"
    end
    return contents[:header].join(',') + "\n" +
           contents[:data].collect{|d| d.join(',') + "\n"}.join()
  end

  private
  def parse(data)
    contents = {header: nil, data: []}
    html = Nokogiri::HTML(data)
    html.search('table#tablefix1').each do |elem|
      rows   = elem.search('tr').to_a
      contents[:header] = parse_header(rows.shift(2))

      rows.each do |row|
        contents[:data].push(row.search('td').collect {|dat| dat.text})
      end
    end
    return contents
  end

  def parse_header(hrows)
    header = []
    hrow2  = hrows[1].search('th')
    hrows[0].search('th').each do |head|
      if head['colspan']
        head['colspan'].to_i.times do
          h = hrow2.shift
          header.push(sprintf("%s[%s]", head.text, h.text))
        end
      else
        header.push(head.text)
      end
    end
    return header
  end
  
  def cache_path(prec_no, block_no, year, month, day)
    return File.join(CACHE_DIR, prec_no, block_no, sprintf("%4d/%02d_%02d.html", year, month, day))
  end

  def cached?(prec_no, block_no, year, month, day)
    return File.exist?(cache_path(prec_no, block_no, year, month, day))
  end

  def get_html_cache(prec_no, block_no, year, month, day)
    open(cache_path(prec_no, block_no, year, month, day)) do |cache|
      return cache.read
    end
  end

  def get_html(prec_no, block_no, year, month, day)
    FileUtils.mkpath(File.dirname(cache_path(prec_no, block_no, year, month, day)))
    base =  block_no.length >= 5 ? BASE_S_URL : BASE_A_URL
    open(cache_path(prec_no, block_no, year, month, day), "w") do |cache|
      open(base + "prec_no=#{prec_no}&block_no=#{block_no}&year=#{year}&month=#{month}&day=#{day}&view=") do |uri|
        html = uri.read
        cache.write(html)
        return html
      end
    end
  end
end

class RegionData
  # 地方コード一覧を取得する
  # ・都道府県コード一覧(prec_no)
  # https://www.data.jma.go.jp/obd/stats/etrn/select/prefecture00.php
  # ・ブロックコード一覧(block_no)
  # https://www.data.jma.go.jp/obd/stats/etrn/select/prefecture.php?prec_no=11

  PREC_URL  = "https://www.data.jma.go.jp/obd/stats/etrn/select/prefecture00.php"
  BLOCK_URL = "https://www.data.jma.go.jp/obd/stats/etrn/select/prefecture.php"
  CACHE_FILE = File.join(File.dirname(__FILE__), 'region.csv')
  
  def search(name)
    unless cached?
      get()
    end
    
    open(CACHE_FILE).each_line do |line|
      if line.match(name)
        return line.split(',')
      end
    end
    return nil
  end

  def cached?
    return File.exist?(CACHE_FILE)
  end
  
  def get
    html = Nokogiri::HTML(open(PREC_URL).read)
    open(CACHE_FILE, 'w') do |csv|
      html.search('area').each do |area|
        prec_name = area['alt']
        prec_no = area['href'].match(/prec_no=(\d+)/)[1]
        prec_html = Nokogiri::HTML(open(BLOCK_URL + "?prec_no=#{prec_no}").read)

        list = []
        prec_html.search('area').each do |parea|
          next unless parea['onmouseover']
          block_name = parea['alt']
          block_no   = parea['href'].match(/block_no=(\d+)/)[1]
          block_data = parea['onmouseover'].match(/viewPoint\((.+)\)/)[1].tr("'", '')
          dat = sprintf("%s,%s,%s,%s,%s\n", prec_no, block_no, prec_name, block_name, block_data)
          list.push(dat)
        end
        list.uniq.each do |line|
          csv.puts(line)
        end
        sleep(rand(1)+2)
      end
    end
  end
end

region = RegionData.new
unless region.cached?
  STDERR.puts("地域一覧を取得しています。しばらくお待ちください。")
  region.get
end

date     = nil
prec_id  = nil
block_id = nil
force    = false

opt = OptionParser.new
opt.on('-d DATE',     '例: 320190920', Date){|d| date = d}
opt.on('-p PREC_ID',  '例: 99'){|pid|}
opt.on('-b BLOCK_ID', '例: 89532'){|bid|}
opt.on('-f',          'キャッシュを無視して再取得'){|f| force = true}
opt.on('-r REGION',   '例: 昭和'){|r|
  if res = region.search(r)
    STDERR.puts(res.join(' '))
    prec_id = res[0]
    block_id = res[1]
  end
}
opt.parse!

unless date && prec_id && block_id
  STDERR.puts(opt.help)
  exit 1
end

md = MeteorologicalData.new
puts(md.get_csv(prec_id, block_id, date.year.to_s, date.month.to_s, date.day.to_s, force: force))
